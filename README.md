const  { EC2Client,DescribeVolumesCommand,CreateSnapshotCommand,DescribeSnapshotsCommand, DeleteSnapshotCommand } = require( "@aws-sdk/client-ec2");

// Set the AWS Region.
const REGION = "ap-south-1"; //e.g. "us-east-1"
// Create anAmazon EC2 service client object.
const ec2Client = new EC2Client({ region: REGION });


const {
    CreateTagsCommand,
    RunInstancesCommand
} = require("@aws-sdk/client-ec2");

// Set the parameters
const instanceParams = {
    ImageId: "ami-0b6fd493a04fd7d2a", //AMI_ID
    InstanceType: "t2.micro",
    KeyName: "MKN", //KEY_PAIR_NAME
    MinCount: 1,
    MaxCount: 1,
};

const run = async () => {
    try {

        do{
            //List all EBS 
            const data = await ec2Client.send(new DescribeVolumesCommand({}));
            console.log(data);
            resCreateSnapshot = await ec2Client.send(new CreateSnapshotCommand({Description:"AUTO_SNAPSHOT", DryRun:false, VolumeId: data.Volumes[3].VolumeId}));
            describeSnapshot = await ec2Client.send(new DescribeSnapshotsCommand({OwnerIds:["self"]}));
            let dateNow = new Date();
            for await (const snapshot of describeSnapshot.Snapshots) {
                let dateCreateSnapshot = new Date(snapshot.StartTime);
                var diffDays = parseInt((dateNow - dateCreateSnapshot) / (1000 * 60 * 60 * 24));
                // Delete snapshot create before 30 days
                if(diffDays==0){
                    resDeleteSnapshot = await ec2Client.send(new DeleteSnapshotCommand({SnapshotId: snapshot.SnapshotId}));
                    console.log(resDeleteSnapshot);
                }
            }

        }while(data && data.NextToken!=undefined);
        //Create Snapshot
        


    } catch (err) {
        console.log("Error", err);
    }
};
run();

